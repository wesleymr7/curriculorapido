const port = process.env.PORT || 8080
const app = require('./src/app')

app.listen(port, () => {
    console.log('Curriculo rápido running', port)
})
//module.exports = app