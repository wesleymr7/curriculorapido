const utils = require('../utils/functions')
const template1 = require('../htmls/template1')
const template2 = require('../htmls/template2')
const template3 = require('../htmls/template3')
const template4 = require('../htmls/template4')
const template5 = require('../htmls/template5')


const createResumeFromTemplate1 = async(body) => {
    return new Promise(async (resolve, reject) => {
        try {
            var template = template1.getTemplate(body)
            const resume  = await utils.createPdf(template)
            resolve(resume)
        } catch(e) {
            reject(e)
        }

    })
    
}
const createResumeFromTemplate2 = async(body) => {
    return new Promise(async (resolve, reject) => {
        try {
            var template = template2.getTemplate(body)
            const resume  = await utils.createPdf(template)
            resolve(resume)
        } catch(e) {
            reject(e)
        }

    })
}
const createResumeFromTemplate3 = async(body) => {
    return new Promise(async (resolve, reject) => {
        try {
            var template = template3.getTemplate(body)
            const resume  = await utils.createPdf(template)
            resolve(resume)
        } catch(e) {
            reject(e)
        }

    })
    
}
const createResumeFromTemplate4 = async(body, image) => {
    return new Promise(async (resolve, reject) => {
        try {
            var template = template4.getTemplate(body, image)
            const resume  = await utils.createPdf(template)
            resolve(resume)
        } catch(e) {
            reject(e)
        }

    })
    
}
const createResumeFromTemplate5 = async(body, image) => {
    return new Promise(async (resolve, reject) => {
        try {
            var template = template5.getTemplate(body, image)
            const resume  = await utils.createPdf(template)
            resolve(resume)
        } catch(e) {
            reject(e)
        }

    })
    
}

/*const createResumeFromTemplate4 = (body, image) => {
    return new Promise((resolve, reject) => {
        var template = template4.getTemplate(body, image)
        utils.createPdfBinary(template, function (binary) {
            resolve(binary)
        }, function (error) {
            reject(error)
        });
    })
    
}*/
module.exports = {
    createResumeFromTemplate1,
    createResumeFromTemplate2,
    createResumeFromTemplate3,
    createResumeFromTemplate4,
    createResumeFromTemplate5
}