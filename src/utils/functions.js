var pdfMakePrinter = require('pdfmake/src/printer');
var pdf = require('html-pdf')
const path = require('path')
const createPdfBinary = (pdfDoc, callback) => {

    var fontDescriptors = {
        Roboto: {
            normal: path.join(__dirname, '..', 'templates', '/fonts/Roboto-Regular.ttf'),
            bold: path.join(__dirname, '..', 'templates', '/fonts/Roboto-Medium.ttf'),
            italics: path.join(__dirname, '..', 'templates', '/fonts/Roboto-Italic.ttf'),
            bolditalics: path.join(__dirname, '..', 'templates', '/fonts/Roboto-MediumItalic.ttf')
        }
    };
    var printer = new pdfMakePrinter(fontDescriptors);

    var doc = printer.createPdfKitDocument(pdfDoc);

    var chunks = [];
    var result;

    doc.on('data', function (chunk) {
        chunks.push(chunk);
    });
    doc.on('end', function () {
        result = Buffer.concat(chunks);
        callback(result)
        //callback('data:application/pdf;base64,' + result.toString('base64'));
        //callback(result.toString('base64'))
    });
    doc.end();

}

const createPdf = (html) => {
    return new Promise((resolve, reject) => {
        pdf.create(html).toBuffer((err, buffer) => {
           if(err) {
               reject(err)
           } else {
               resolve(buffer.toString('base64'))
               //resolve(buffer)
           }
        })
    })
}

module.exports = {
    createPdfBinary,
    createPdf
}