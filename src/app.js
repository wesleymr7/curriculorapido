


/*app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
app.use(express.static('public'))*/


const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')

const routes = require('./routes')
app.use(express.static('public'))
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(routes)
module.exports = app


