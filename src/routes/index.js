const router = require('express').Router()

const resumeRouter = require('./resume')

router.use('/api/v1/resume', resumeRouter)

module.exports = router