const router = require('express').Router()
const resumeController = require('../controllers/resume')
const multer = require('multer')
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

router.post('/1', upload.single('image'), resumeController.createFromTemplate1)
router.post('/2', upload.single('image'), resumeController.createFromTemplate2)
router.post('/3', upload.single('image'), resumeController.createFromTemplate3)
router.post('/4', upload.single('image'), resumeController.createFromTemplate4)
router.post('/5', upload.single('image'), resumeController.createFromTemplate5)

module.exports = router
