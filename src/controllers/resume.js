const resumeModel = require('../models/resume')

const createFromTemplate1 = async (req, res) => {
    try {
        const resume = await resumeModel.createResumeFromTemplate1(req.body)
        res.contentType('application/pdf');
        //res.send(resume)
        res.send({
            status: 200,
            data: resume
        })
    } catch (error) {
        console.log(error)
        res.send({
            status: 400,
            data: error
        })
    }
}
const createFromTemplate2 = async (req, res) => {
    try {
        const resume = await resumeModel.createResumeFromTemplate2(req.body)
        res.contentType('application/pdf');
        //res.send(resume)
        res.send({
            status: 200,
            data: resume
        })
    } catch (error) {
        console.log(error)
        res.send({
            status: 400,
            data: error
        })
    }
}
const createFromTemplate3 = async (req, res) => {
    try {
        const resume = await resumeModel.createResumeFromTemplate3(req.body)
        res.contentType('application/pdf');
        //res.send(resume)
        res.send({
            status: 200,
            data: resume
        })
    } catch (error) {
        console.log(error)
        res.send({
            status: 400,
            data: error
        })
    }
}

const createFromTemplate4 = async (req, res) => {
    try {
        let image = null
        if(req.file) image = 'data:image/png;base64,' + req.file.buffer.toString('base64')
        const resume = await resumeModel.createResumeFromTemplate4(req.body, image)
        res.contentType('application/pdf');
        //res.send(resume)
        res.send({
            status: 200,
            data: resume
        })
    } catch (error) {
        console.log(error)
        res.send({
            status: 400,
            data: error
        })
    }
}
const createFromTemplate5 = async (req, res) => {
    try {
        console.log(req.body)
        if(req.file) image = 'data:image/png;base64,' + req.file.buffer.toString('base64')
        const resume = await resumeModel.createResumeFromTemplate5(req.body, image)
        res.contentType('application/pdf');
        //res.send(resume)
        res.send({
            status: 200,
            data: resume
        })
    } catch (error) {
        console.log(error)
        res.send({
            status: 400,
            data: error
        })
    }
}

/*const createFromTemplate4 = async (req, res) => {
    try {
        let image = null
        if(req.file) image = 'data:image/png;base64,' + req.file.buffer.toString('base64')
        const resume = await resumeModel.createResumeFromTemplate4(req.body, image)
        res.contentType('application/pdf');
        res.send({
            status: 200,
            data: resume
        })
    } catch (error) {
        console.log(error)
        res.send({
            status: 400,
            data: error
        })
    }
}*/


module.exports = {
    createFromTemplate1,
    createFromTemplate2,
    createFromTemplate3,
    createFromTemplate4,
    createFromTemplate5
}