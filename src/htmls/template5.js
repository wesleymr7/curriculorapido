const moment = require('moment')
const getTemplate = (body, image) => {
    let schoolings = (body.schoolings) ? JSON.parse(body.schoolings): null
    let courses = (body.courses) ? JSON.parse(body.courses): null
    let experiences = (body.experiences) ? JSON.parse(body.experiences): null
    let extra_infos = (body.extra_infos) ? JSON.parse(body.extra_infos): null
    let languages = (body.languages) ? JSON.parse(body.languages): null
    var { name, cep, street, number, district, city, state, phone1, phone2, birthday, civil_state, email, goal, nationality } = body
    if (!name || name == 'null') name = ''
    if (!cep || cep == 'null') cep = ''
    if (!street || street == 'null') street = ''
    if (!number || number == 'null') number = ''
    if (!district || district == 'null') district = ''
    if (!city || city == 'null') city = ''
    if (!state || state == 'null') state = ''
    if (!phone1 || phone1 == 'null') phone1 = ''
    if (!phone2 || phone2 == 'null') phone2 = ''
    if (!birthday || birthday == 'null') birthday = ''
    if (!civil_state || civil_state == 'null') civil_state = ''
    if (!email || email == 'null') email = ''
    if (!nationality || nationality == 'null') nationality = ''
    if (!goal || goal == 'null') goal = ''
    birthday = (birthday) ? moment.utc(birthday).format('DD/MM/YYYY') : ''
    const address = `${street ? street : ''}${number ? ', ' + number : ''} ${district ? '- ' + district : ''} ${city ? '- ' + city : ''} ${state ? '- ' + state : ''} ${cep ? '- ' + cep: ''}`
    let textSchoolings = ''
    let textCourses = ''
    let textExperiences = ''
    let textExtraInfos = ''
    let textLanguages = ''
    schoolings && schoolings.schoolings.forEach(s => {
        var { status, level, course, instituition } = s
        level += ' - '
        status += ' | '
        course += ' - '
        if (s.level == '' || !s.level) level = 'Nível '
        if (s.status == '' || !s.status) status = 'Status '
        if ((s.course == '' || !s.course) && (s.level == 'Ensino Fundamental' || s.level == 'Ensino Médio')) course = ''
        if ((s.course == '' || !s.course) && (s.level !== 'Ensino Fundamental' || s.level !== 'Ensino Médio')) course = 'Curso'
        if (s.instituition == '' || !s.instituition) instituition = 'Instituição'
        textSchoolings += `<li>${level} - ${status} | ${course} - ${instituition} </li>`
    })
    courses && courses.courses.forEach(c => {
        var { course, instituition, year } = c
        if (c.course == '' || !c.course) course = ''
        if (c.instituition == '' || !c.instituition) instituition = ''
        textCourses += `<li>${course} -- ${instituition} (${year}) </li>`
    })
    languages && languages.languages.forEach(l => {
        var { language, write, read, speak } = l
        textLanguages += `<li><strong>${language}</strong> -- Escrita: ${write} | Leitura: ${read} | Conversação: ${speak}</li>`
    })
    experiences && experiences.experiences.forEach(e => {
        textExperiences += '<li>'
        textExperiences += `<p><span style="font-weight: bold;">${e.local} </span>(${e.init}) - (${e.end})</p>`
        textExperiences += `<p>${e.function}</p>`,
            textExperiences += "<ul>"
        e.items.forEach(i => {
            textExperiences += `<li><span class="">${i}</span></li>`
        })
        textExperiences += '</ul>'
        textExperiences += '</li>'
    })
    extra_infos && extra_infos.extra_infos.forEach(i => {
        textExtraInfos += `<li>${i}</li>`
    })

    const divGoal = (goal) ? `  <tr>
                                    <td width="25%">
                                        <h3 class="title">OBJETIVO</h3>
                                        <div class="content">
                                            <ul>
                                                <li>${goal}</li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>` : ''
    const divSchoolings = (textSchoolings) ? `  <tr>
                                                    <td width="25%">
                                                        <h3 class="title">ESCOLARIDADE</h3>
                                                        <div class="content">
                                                            <ul>
                                                                ${textSchoolings}
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>`: ''

    const divCourses = (textCourses) ? `<tr>
                                            <td width="25%">
                                                <h3 class="title">CURSOS</h3>
                                                <div class="content">
                                                    <ul>
                                                        ${textCourses}
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>` : ''
    const divLanguages = (textLanguages) ? `<tr>
                                                <td width="25%">
                                                    <h3 class="title">IDIOMAS</h3>
                                                    <div class="content">
                                                        <ul>
                                                            ${textLanguages}
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>` : ''
    const divExperiences = (textExperiences) ? `<tr>
                                                    <td width="25%">
                                                        <h3 class="title">EXPERIÊNCIAS PROFISSIONAIS</h3>
                                                        <div class="content">
                                                            <ul>
                                                                ${textExperiences}
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>` : ''
    const divExtraInfos = (textExtraInfos) ? `  <tr>
                                                    <td width="25%">
                                                        <h3 class="title">INFORMAÇÕES ADICIONAIS</h3>
                                                        <div class="content">
                                                            <ul>
                                                                ${textExtraInfos}
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>` : ''
    return docDefinition = `
    <html>
    <head>
      <meta charset="utf8">
      <title>SuitArt Business Card</title>
      <style>
        html {
            zoom: 0.55;
        }
        html, body {
          margin: 0;
          padding: 0;
          font-family: 'Arial';
          font-size: 12pt;
          -webkit-print-color-adjust: exact;
          box-sizing: border-box;
        }
        .page {
          position: relative;
          height: 1122.51px;
          width: 793.7px;
          display: block;
          page-break-after: auto;
          margin: 0;
          padding:0;
          overflow: hidden;
        }
        @media print {
            body {
                margin: 0;
            }
            .page {
                margin: 80px 76px 76px 80px;
                height: auto;
                width: 100%;
            }
        }
        .name {
            text-align: center;
            text-transform: uppercase;
        }
        .picture {
            height: 150px;
            border: 1px solid #d8d8d8;
            width: 90%;
            margin-left: 6px;
            margin-top: -23px;
        }
        .personal-info {
            font-size: 11pt;
            padding-left: 15px;
        }
        .personal-info h2 {
            font-size: 15.5pt;
        }
        .personal-info p {
            margin-top: -20px;
            font-size: 12pt;
        }
        .personal-info strong {            
            font-size: 12pt;
        }
        .box-content {
            width: 80%;
            padding: 5px 0;
        }
        .box-content .title {
            font-size: 12pt;            
            font-weight: bold;
            padding: 3px;
            text-align: center;
            border: 1px solid black;
            background: #d8d8d8;
        }
        .box-content .content {
            font-size: 12pt;
        }
        .box-content p {
            height: auto;
            font-size: 12pt;
        }
        .box-content .date {
            font-size: 10pt;
        }
        .box-content .local {
            margin-bottom: 10px;
            font-size:12pt;
            color: rgb(21, 28, 58);
        }
        .box-content .item {
            font-size: 10pt;
        }
        }
      </style>
    </head>
    <body>
        <div class="page">
            <table border="0" style="width: 80%;">
                <tr>
                    <td width="85%">
                        <h2 class="name">${name}</h2>
                    </td>
                    <td width="15%" >
                    </td>
                </tr>
            </table>
            <div class="box-content" >
                <table border="0" width="100%">
                    <tr>
                        <td width="83%">
                            <h3 class="title" style="margin-right: 10px;">DADOS PESSOAIS</h3>
                            <ul>
                                <li><strong>Endereço: </strong>${address}</li>
                                <li><strong>Telefones: </strong>${phone1} | ${phone2 ? ' ! ' + phone2 : ''}</li>
                                <li><strong>E-mail: </strong>${email}</li>
                                <li><strong>Estado civil: </strong>${civil_state}</li>
                                <li><strong>Data de nascimento: </strong>${birthday}</li>
                                <li><strong>Nacionalidade: </strong>${nationality}</li>
                            </ul>
                        </td>
                        <td width="17%" >
                            <div style="position:relative; height: 100%">
                                <img src="${image}"  class="picture" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="box-content">
                <table border="0" width="100%">
                    ${divGoal}
                    ${divSchoolings}
                    ${divCourses}
                    ${divLanguages}
                    ${divExperiences}
                    ${divExtraInfos}
                </table>
            </div>
        </div>
    </body>
  </html>
    `
}
module.exports = {
    getTemplate
};