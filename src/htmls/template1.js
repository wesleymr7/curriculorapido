const moment = require('moment')
const getTemplate = (body) => {
    let schoolings = (body.schoolings) ? JSON.parse(body.schoolings) : null
    let courses = (body.courses) ? JSON.parse(body.courses) : null
    let experiences = (body.experiences) ? JSON.parse(body.experiences) : null
    let extra_infos = (body.extra_infos) ? JSON.parse(body.extra_infos) : null
    let languages = (body.languages) ? JSON.parse(body.languages) : null
    var { name, cep, street, number, district, city, state, phone1, phone2, birthday, civil_state, email, goal } = body
    if (!name || name == 'null') name = ''
    if (!cep || cep == 'null') cep = ''
    if (!street || street == 'null') street = ''
    if (!number || number == 'null') number = ''
    if (!district || district == 'null') district = ''
    if (!city || city == 'null') city = ''
    if (!state || state == 'null') state = ''
    if (!phone1 || phone1 == 'null') phone1 = ''
    if (!phone2 || phone2 == 'null') phone2 = ''
    if (!birthday || birthday == 'null') birthday = ''
    if (!civil_state || civil_state == 'null') civil_state = ''
    if (!email || email == 'null') email = ''
    if (!goal || goal == 'null') goal = ''
    birthday = (birthday) ? moment.utc(birthday).format('DD/MM/YYYY') : ''
    const address = `${street ? street : ''}${number ? ', ' + number : ''} ${district ? '- ' + district : ''} ${city ? '- ' + city : ''} ${state ? '- ' + state : ''} ${cep ? '- ' + cep: ''}`
    let textSchoolings = ''
    let textCourses = ''
    let textExperiences = ''
    let textExtraInfos = ''
    let textLanguages = ''

    schoolings && schoolings.schoolings.forEach(s => {
        var { status, level, course, instituition } = s
        level += ' - '
        status += ' | '
        course += ' - '
        if (s.level == '' || !s.level) level = 'Nível - '
        if (s.status == '' || !s.status) status = 'Status | '
        if ((s.course == '' || !s.course) && (s.level == 'Ensino Fundamental' || s.level == 'Ensino Médio')) course = ''
        if ((s.course == '' || !s.course) && (s.level !== 'Ensino Fundamental' || s.level !== 'Ensino Médio')) course = 'Curso'
        if (s.instituition == '' || !s.instituition) instituition = 'Instituição'
        textSchoolings += `<li>${level} ${status} ${course} ${instituition}</li>`
    })
    courses && courses.courses.forEach(c => {
        var { course, instituition, year } = c
        instituition = ' - ' + instituition
        if (c.course == '' || !c.course) course = ''
        if (c.instituition == '' || !c.instituition) instituition = ''
        textCourses += `<li>${course} ${instituition} (${year})</li>`
    })
    languages && languages.languages.forEach(l => {
        var { language, write, read, speak } = l
        textLanguages += `<li>${language} - Escrita: ${write} | Leitura: ${read} | Conversação: ${speak}</li>`
    })
    experiences && experiences.experiences.forEach(e => {
        textExperiences += `<span class="date">(${e.init}) (${e.end ? e.end : 'Atualmente'}) </span><br>`,
            textExperiences += `<span class="local">${e.local} - ${e.function} </span><br><br>`
        e.items.forEach(i => {
            textExperiences += `<span class="item">- ${i}</span><br>`
        })
    })
    extra_infos && extra_infos.extra_infos.forEach(i => {
        textExtraInfos += `<span class="item"> - ${i} </span><br>`
    })
    const divGoal = (goal) ? `  <div class="box-content">
                                    <h3>Objetivo</h3>
                                    <p>
                                        ${goal}
                                    <p>
                                </div>` : ''
    const divSchoolings = (textSchoolings) ? `  <div class="box-content">
                                                    <h3>Formação</h3>
                                                    <ul>${textSchoolings}</ul>
                                                </div>`: ''

    const divCourses = (textCourses) ? `<div class="box-content">
                                            <h3>Cursos</h3>
                                            <ul>${textCourses}</ul>
                                        </div>` : ''
    const divLanguages = (textLanguages) ? `<div class="box-content">
                                                <h3>Idiomas</h3>
                                                <ul>${textLanguages}</ul>
                                            </div>` : ''
    const divExperiences = (textExperiences) ? `<div class="box-content">
                                                    <h3>Experiência</h3>
                                                    ${textExperiences}
                                                </div>` : ''
    const divExtraInfos = (textExtraInfos) ? `<div class="box-content">
                                                    <h3>Informações Adicionais</h3>
                                                    ${textExtraInfos}
                                                </div>` : ''
    return docDefinition = `
    <html>
    <head>
      <meta charset="utf8">
      <title>SuitArt Business Card</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <style>
        html {
           zoom: 0.55;
        }
        html, body {
          margin: 0;
          padding: 0;
          font-family: 'Arial';
          font-size: 12pt;
          -webkit-print-color-adjust: exact;
          box-sizing: border-box;
        }
        .page {
          position: relative;
          height: 1122.51px;
          width: 793.7px;
          display: block;
          page-break-after: auto;
          margin: 0;
          padding: 0;
          overflow: hidden;
        }
        @media print {
            body {
                margin: 0;
            }
            .page {
                margin: 80px 76px 76px 80px;
                height: auto;
                width: 100%;
            }
          }
        .name {
            width: 80%;
            font-size: 34pt;
            margin-bottom: 5px;
            color: rgb(21, 28, 58);
        }
        .personal-info {
            width: 80%;
            font-size: 11pt;
            margin-bottom: 50px;
            color: rgb(18, 96, 106);
        }
        .box-content {
            width: 80%;
            margin-top: 30px,
            margin-bottom: 30px;
            color: rgb(89, 89, 89);
            font-size: 11pt;
        }
        .box-content h3 {
            font-size: 17pt;
            color: rgb(21, 28, 58);
            margin-top: 5px;
        }
        .box-content p {
            width: 80%;
            height: auto;
            font-size: 15pt;
        }
        .box-content .date {
            color: rgb(18, 96, 106);
            font-size: 11pt;
        }
        .box-content .local {
            margin-bottom: 10px;
            font-size:12pt;
            color: rgb(21, 28, 58);
        }
        .box-content .item {
            font-size: 11pt;
        }
        }
      </style>
    </head>
    <body>
      <div class="page">
        <h1 class="name">${name}</h1>
        <div class="personal-info">
            ${address ? address : ''}  ${city ? '| ' + city: ''}  ${state ? '- ' + state : ''} ${phone1 ? '| ' + phone1 : ''} ${phone2 ? '| ' + phone2 + ' |' : ''} ${email}
        </div>
        ${divGoal}
        ${divSchoolings}
        ${divCourses}
        ${divLanguages}
        ${divExperiences}
        ${divExtraInfos}
      </div>
    </body>
  </html>
    `
}
module.exports = {
    getTemplate
};