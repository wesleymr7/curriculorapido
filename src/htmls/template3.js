const moment = require('moment')
const getTemplate = (body) => {
    let schoolings = (body.schoolings) ? JSON.parse(body.schoolings): null
    let courses = (body.courses) ? JSON.parse(body.courses): null
    let experiences = (body.experiences) ? JSON.parse(body.experiences): null
    let extra_infos = (body.extra_infos) ? JSON.parse(body.extra_infos): null
    let languages = (body.languages) ? JSON.parse(body.languages): null
    var { name, cep, street, number, district, city, state, phone1, phone2, birthday, civil_state, email, goal } = body
    if (!name || name == 'null') name = ''
    if (!cep || cep == 'null') cep = ''
    if (!street || street == 'null') street = ''
    if (!number || number == 'null') number = ''
    if (!district || district == 'null') district = ''
    if (!city || city == 'null') city = ''
    if (!state || state == 'null') state = ''
    if (!phone1 || phone1 == 'null') phone1 = ''
    if (!phone2 || phone2 == 'null') phone2 = ''
    if (!birthday || birthday == 'null') birthday = ''
    if (!civil_state || civil_state == 'null') civil_state = ''
    if (!email || email == 'null') email = ''
    if (!goal || goal == 'null') goal = ''
    birthday = (birthday) ? moment.utc(birthday).format('DD/MM/YYYY') : ''
    const address = `${street ? street : ''}${number ? ', ' + number : ''} ${district ? '- ' + district : ''} ${city ? '- ' + city : ''} ${state ? '- ' + state : ''} ${cep ? '- ' + cep: ''}`
    let textSchoolings = ''
    let textCourses = ''
    let textExperiences = ''
    let textExtraInfos = ''
    let textLanguages = ''

    schoolings && schoolings.schoolings.forEach(s => {
        var { status, level, course, instituition } = s
        level += ' - '
        status += ' | '
        course += ' - '
        if (s.level == '' || !s.level) level = 'Nível '
        if (s.status == '' || !s.status) status = 'Status '
        if ((s.course == '' || !s.course) && (s.level == 'Ensino Fundamental' || s.level == 'Ensino Médio')) course = ''
        if ((s.course == '' || !s.course) && (s.level !== 'Ensino Fundamental' || s.level !== 'Ensino Médio')) course = 'Curso'
        if (s.instituition == '' || !s.instituition) instituition = 'Instituição'
        textSchoolings += `<p style="font-weight: bold; margin-bottom:15px;">${level} em ${course}</p><p> ${instituition} | ${status}</p>`
    })
    courses && courses.courses.forEach(c => {
        var { course, instituition, year } = c
        if (c.course == '' || !c.course) course = ''
        if (c.instituition == '' || !c.instituition) instituition = ''
        textCourses += `<span style="color: #4169E1;font-weight: bold;">${instituition}</span> -- ${course} (${year})`
    })
    languages && languages.languages.forEach(l => {
        var { language, write, read, speak } = l
        textLanguages += `<span>${language}</span> -- Escrita: ${write} | Leitura: ${read} | Conversação: ${speak}</li>`
    })
    experiences && experiences.experiences.forEach(e => {
        textExperiences += `<p><span style="font-weight: bold;">${e.local} </span>(${e.init}) - (${e.end})</p>`
        textExperiences += `<p>${e.function}</p>`,
            textExperiences += "<ul>"
        e.items.forEach(i => {
            textExperiences += `<li><span class="item">${i}</span></li>`
        })
        textExperiences += '</ul>'
    })
    extra_infos && extra_infos.extra_infos.forEach(i => {
        textExtraInfos += `<p> - ${i} </p>`
    })

    const divGoal = (goal) ? `  <div class="box-content">
                                    <p class="title">Objetivo</p>
                                    <p>${goal}</p>
                                </div>` : ''
    const divSchoolings = (textSchoolings) ? `  <div class="box-content">
                                                    <p class="title">Formação Acadêmica</p>
                                                    ${textSchoolings}
                                                </div>`: ''

    const divCourses = (textCourses) ? `<div class="box-content">
                                            <p class="title">Cursos</h3>
                                            ${textCourses}
                                        </div>` : ''
    const divLanguages = (textLanguages) ? `<div class="box-content">
                                                <p class="title">Idiomas</p>
                                                ${textLanguages}
                                            </div>` : ''
    const divExperiences = (textExperiences) ? `<div class="box-content">
                                                    <p class="title">Experiência Profissional</p>
                                                    ${textExperiences}
                                                </div>` : ''
    const divExtraInfos = (textExtraInfos) ? `<div class="box-content">
                                                <p class="title">Informações Adicionais</p>
                                                ${textExtraInfos}
                                            </div>` : ''
    return docDefinition = `
    <html>
    <head>
      <meta charset="utf8">
      <title>SuitArt Business Card</title>
      <style>
        html {
            zoom: 0.55;
        }
        html, body {
          margin: 0;
          padding: 0;
          font-family: 'Arial';
          font-size: 12pt;
          -webkit-print-color-adjust: exact;
          box-sizing: border-box;
        }
        .page {
          position: relative;
          height: 1122.51px;
          width: 793.7px;
          display: block;
          page-break-after: auto;
          margin: 0;
          padding:0;
          overflow: hidden;
        }
        @media print {
            body {
                margin: 0;
            }
            .page {
                margin: 80px 76px 76px 80px;
                height: auto;
                width: 100%;
            }
          }
        .head {
        }
        .name {
            font-size: 15.5pt;
            color: rgb(15,111, 198);
            font-weight: normal;
            font-style: normal;
        }
        .personal-info-1 {
            margin-left: 150px;
            text-align: 'right';
            font-size: 9pt;
            font-style: italic;
        }
        .personal-info-2 {
            margin-left: 100px;
            text-align: 'right';
            font-size: 9pt;
            font-style: italic;
            font-weight: bold;
        }
        .box-content {
            width: 80%;
            margin-top: 50px,
            margin-bottom: 30px;
            color: #000;
            font-size: 10pt;
        }
        .box-content .title {
            width:100%;
            font-size: 11pt;
            padding-bottom: 2px;
            font-style: italic;
            color: rgb(15,111, 198);
            border-bottom: 1px solid #000;
        }
        .box-content p {
            width: 100%;
            height: auto;
            font-size: 11pt;
            margin-top: 20px;
        }
        .box-content .date {
            font-size: 10pt;
        }
        .box-content .local {
            margin-bottom: 10px;
            font-size:12pt;
            color: rgb(21, 28, 58);
        }
        .box-content .item {
            font-size: 10pt;
        }
        }
      </style>
    </head>
    <body>
        <div class="page">
            <table border="0" style="width: 80%;">
                <tr>
                    <td width="50%">
                        <div class="name">
                            <span>${name}</span>
                        </div>
                    </td>
                    <td width="50%" style="border-bottom: 1px solid #000;">
                        <div class="personal-info-1">
                            <p>${birthday}
                            ${address}</p>
                        </div>
                        <div class="personal-info-2">
                            <p>${phone1 ? phone1 + ' |': ''} ${phone2 ? phone2 + ' |': ''} ${email} </p>
                        </div>
                    </td>
                </tr>
            </table>
            ${divGoal}
            ${divSchoolings}
            ${divCourses}
            ${divLanguages}
            ${divExperiences}
            ${divExtraInfos}
        </div>
    </body>
  </html>
    `
}
module.exports = {
    getTemplate
};