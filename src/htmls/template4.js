const moment = require('moment')
const getTemplate = (body, image) => {
    let schoolings = (body.schoolings) ? JSON.parse(body.schoolings): null
    let courses = (body.courses) ? JSON.parse(body.courses): null
    let experiences = (body.experiences) ? JSON.parse(body.experiences): null
    let extra_infos = (body.extra_infos) ? JSON.parse(body.extra_infos): null
    let languages = (body.languages) ? JSON.parse(body.languages): null
    var { name, cep, street, number, district, city, state, phone1, phone2, birthday, civil_state, email, goal } = body
    if (!name || name == 'null') name = ''
    if (!cep || cep == 'null') cep = ''
    if (!street || street == 'null') street = ''
    if (!number || number == 'null') number = ''
    if (!district || district == 'null') district = ''
    if (!city || city == 'null') city = ''
    if (!state || state == 'null') state = ''
    if (!phone1 || phone1 == 'null') phone1 = ''
    if (!phone2 || phone2 == 'null') phone2 = ''
    if (!birthday || birthday == 'null') birthday = ''
    if (!civil_state || civil_state == 'null') civil_state = ''
    if (!email || email == 'null') email = ''
    if (!goal || goal == 'null') goal = ''
    birthday = (birthday) ? moment.utc(birthday).format('DD/MM/YYYY') : ''
    const address = `${street ? street : ''}${number ? ', ' + number : ''} ${district ? '- ' + district : ''} ${city ? '- ' + city : ''} ${state ? '- ' + state : ''} ${cep ? '- ' + cep: ''}`
    let textSchoolings = ''
    let textCourses = ''
    let textExperiences = ''
    let textExtraInfos = ''
    let textLanguages = ''
    schoolings && schoolings.schoolings.forEach(s => {
        var { status, level, course, instituition } = s
        level += ' - '
        status += ' | '
        course += ' - '
        if (s.level == '' || !s.level) level = 'Nível '
        if (s.status == '' || !s.status) status = 'Status '
        if ((s.course == '' || !s.course) && (s.level == 'Ensino Fundamental' || s.level == 'Ensino Médio')) course = ''
        if ((s.course == '' || !s.course) && (s.level !== 'Ensino Fundamental' || s.level !== 'Ensino Médio')) course = 'Curso'
        if (s.instituition == '' || !s.instituition) instituition = 'Instituição'
        textSchoolings += `<p>${level} - ${status} | ${course} - ${instituition} </p>`
    })
    courses && courses.courses.forEach(c => {
        var { course, instituition, year } = c
        if (c.course == '' || !c.course) course = ''
        if (c.instituition == '' || !c.instituition) instituition = ''
        textCourses += `<p>${course} -- ${instituition} (${year}) </p>`
    })
    languages && languages.languages.forEach(l => {
        var { language, write, read, speak } = l
        textLanguages += `<p><strong>${language}</strong> -- Escrita: ${write} | Leitura: ${read} | Conversação: ${speak}</p>`
    })
    experiences && experiences.experiences.forEach(e => {
        textExperiences += `<p><span style="font-weight: bold;">${e.local} </span>(${e.init}) - (${e.end})</p>`
        textExperiences += `<p>${e.function}</p>`,
            textExperiences += "<ul>"
        e.items.forEach(i => {
            textExperiences += `<li><span class="item">${i}</span></li>`
        })
        textExperiences += '</ul>'
    })
    extra_infos && extra_infos.extra_infos.forEach(i => {
        textExtraInfos += `<p> - ${i} </p>`
    })

    const divGoal = (goal) ? `  <div class="box-content" >
                                    <table border="0" width="100%">
                                        <tr>
                                            <td width="25%">
                                                <h3 class="title">Objetivo</h3>
                                            </td>
                                            <td width="75%" class="content">
                                                <p>${goal}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>` : ''
    const divSchoolings = (textSchoolings) ? `  <div class="box-content">
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td width="25%">
                                                                <h3 class="title">Escolaridade</h3>
                                                            </td>
                                                            <td width="75%" class="content">
                                                                ${textSchoolings}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>`: ''

    const divCourses = (textCourses) ? `<div class="box-content">
                                            <table border="0" width="100%">
                                                <tr>
                                                    <td width="25%">
                                                        <h3 class="title">Cursos</h3>
                                                    </td>
                                                    <td width="75%" class="content">
                                                        ${textCourses}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>` : ''
    const divLanguages = (textLanguages) ? `<div class="box-content">
                                                <table border="0" width="100%">
                                                    <tr>
                                                        <td width="25%">
                                                            <h3 class="title">Idiomas</h3>
                                                        </td>
                                                        <td width="75%" class="content">
                                                            ${textLanguages}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>` : ''
    const divExperiences = (textExperiences) ? `<div class="box-content">
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td width="25%">
                                                                <h3 class="title">Experiências</h3>
                                                            </td>
                                                            <td width="75%" class="content">
                                                                ${textExperiences}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>` : ''
    const divExtraInfos = (textExtraInfos) ? `<div class="box-content">
                                                <table border="0" width="100%">
                                                    <tr>
                                                        <td width="25%">
                                                            <h3 class="title">Informações Adicionais</h3>
                                                        </td>
                                                        <td width="75%" class="content">
                                                            ${textExtraInfos}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>` : ''
    return docDefinition = `
    <html>
    <head>
      <meta charset="utf8">
      <title>SuitArt Business Card</title>
      <style>
        html {
            zoom: 0.55;
        }
        html, body {
          margin: 0;
          padding: 0;
          font-family: 'Arial';
          font-size: 12pt;
          -webkit-print-color-adjust: exact;
          box-sizing: border-box;
        }
        .page {
          position: relative;
          height: 1122.51px;
          width: 793.7px;
          display: block;
          page-break-after: auto;
          margin: 0;
          padding:0;
          overflow: hidden;
        }
        @media print {
            body {
                margin: 0;
            }
            .page {
                margin: 80px 76px 76px 80px;
                height: auto;
                width: 100%;
            }
        }
        .picture img {
            height: 150px;
            margin-left: 40px;
            width: 60%;
        }
        .personal-info {
            font-size: 12pt;
            
        }
        .personal-info h2 {
            font-size: 18pt;
        }
        .personal-info p {
            margin-top: -20px;
            font-size: 13pt;
        }
        .personal-info strong {            
            font-size: 14pt;
        }
        .box-content {
            width: 80%;
            border-top: 2pt solid black;
            padding: 15px 0;
            margin-top: 10px;
        }
        .box-content .title {
            font-size: 12pt;
            font-style: italic;
            font-weight: bold;
            margin-left: 40px;
        }
        .box-content .content {
            //padding-left: 15px;
        }
        .box-content p {
            height: auto;
            font-size: 11pt;
        }
        .box-content .date {
            font-size: 10pt;
        }
        .box-content .local {
            margin-bottom: 10px;
            font-size:12pt;
            color: rgb(21, 28, 58);
        }
        .box-content .item {
            font-size: 10pt;
        }
        }
      </style>
    </head>
    <body>
        <div class="page">
            <table border="0" style="width: 80%;">
                <tr>
                    <td width="25%" class="picture">
                        <img src="${image}"  />
                    </td>
                    <td width="75%" class="personal-info">
                       <h2>${name}</h2>
                       <p>
                            <strong>Endereço:</strong> ${address} <br>
                            <strong>Telefones:</strong> ${phone1}${phone2 ? ' | ' + $phone2: '' } <br>
                            <strong>Data de nascimento:</strong> ${birthday} <br>
                            <strong>Estado civil:</strong> ${civil_state}<br>
                            <strong>E-mail:</strong> ${email}
                       </p>
                    </td>
                </tr>
            </table>
            ${divGoal}
            ${divSchoolings}
            ${divCourses}
            ${divLanguages}
            ${divExperiences}
            ${divExtraInfos}
        </div>
    </body>
  </html>
    `
}
module.exports = {
    getTemplate
};