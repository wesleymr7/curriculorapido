const moment = require('moment')
const getTemplate = (body, image) => {
    let schoolings = JSON.parse(body.schoolings)
    let courses = JSON.parse(body.courses)
    let experiences = JSON.parse(body.experiences)
    let extra_infos = JSON.parse(body.extra_infos)
    var { name, cep, street, number, district, city, state, phone1, phone2, birthday, civil_state, email, goal } = body
    if (!name) name = ''
    if (!cep) cep = ''
    if (!street) street = ''
    if (!number) number = ''
    if (!district) district = ''
    if (!city) city = ''
    if (!state) state = ''
    if (!phone1) phone1 = ''
    if (!phone2) phone2 = ''
    if (!birthday) birthday = ''
    if (!civil_state) civil_state = ''
    if (!email) email = ''
    if (!goal) goal = ''
    birthday = moment.utc(birthday).format('DD/MM/YYYY')
    const address = `${street}, ${number} - ${district} - ${city} - ${state} - ${cep}`
    let textSchoolings = ''
    let textCourses = ''
    let textExperiences = []
    let textExtraInfos = ''

    schoolings.schoolings.forEach(s => {
        var { status, level, course, instituition } = s
        level += ' - '
        status += ' | '
        course += ' - '
        if (s.level == '' || !s.level) level = 'Nível - '
        if (s.status == '' || !s.status) status = 'Status | '
        if ((s.course == '' || !s.course) && (s.level == 'Ensino Fundamental' || s.level == 'Ensino Médio')) course = ''
        if ((s.course == '' || !s.course) && (s.level !== 'Ensino Fundamental' || s.level !== 'Ensino Médio')) course = 'Curso'
        if (s.instituition == '' || !s.instituition) instituition = 'Instituição'
        textSchoolings += `${level} ${status} ${course} ${instituition} \n`
    })
    courses.courses.forEach(c => {
        var { course, instituition, year } = c
        instituition = ' - ' + instituition
        if (c.course == '' || !c.course) course = ''
        if (c.instituition == '' || !c.instituition) instituition = ''
        textCourses += `${course} ${instituition} (${year}) \n`
    })
    experiences.experiences.forEach(e => {
        textExperiences.push({
            text: `${e.local} (${e.init}) (${e.end}) \n`,
            style: 'headerExperience'
        })
        e.items.forEach(i => {
            textExperiences.push({
                text: i + '\n',
                style: 'description'
            })
        })
    })
    extra_infos.extra_infos.forEach(i => {
        textExtraInfos += `${i} \n`
    })
    return docDefinition = {
        content: [
            {
                style: 'mainStyle',
                table: {
                    widths: [100, '*'],
                    heights: [100],
                    body: [
                        [
                            {
                                // auto-sized columns have their widths based on their content
                                width: '20%',
                                //image: './public/perfil.png',
                                image: (image) ? image : './public/perfil.png',
                                width: 100,
                                height: 100,
                                margin: [0, 0, 0, 10],
                            },
                            {
                                text: [
                                    {
                                        style: 'name',
                                        width: '*',
                                        text: `${name} \n`
                                    },
                                    // Endereço
                                    {
                                        text: 'Endereço: ',
                                        style: 'header'
                                    },
                                    {
                                        text: `${address} \n`,
                                        style: 'description'
                                    },
                                    // Telefones
                                    {
                                        text: 'Telefones: ',
                                        style: 'header'
                                    },
                                    {
                                        text: `${phone1} \n`,
                                        style: 'description'
                                    },
                                    // nascimento
                                    {
                                        text: 'Data de nascimento: ',
                                        style: 'header'
                                    },
                                    {
                                        text: `${birthday} \n`,
                                        style: 'description'
                                    },
                                    // Estado Civil
                                    {
                                        text: 'Estado civil: ',
                                        style: 'header'
                                    },
                                    {
                                        text: `${civil_state} \n`,
                                        style: 'description'
                                    },
                                    // Email
                                    {
                                        text: 'Email: ',
                                        style: 'header'
                                    },
                                    {
                                        text: `${email} \n`,
                                        style: 'description'
                                    }
                                ]
                            }
                        ],
                        [
                            {
                                // auto-sized columns have their widths based on their content
                                style: 'title',
                                text: 'Objetivo',
                                border: [false, true, false, true],
                                //margin: [20, 20],
                            },
                            {
                                text: [
                                    { text: goal, italics: true, style: 'description' },
                                ],
                                border: [false, true, false, true],
                                style: 'description'
                            }
                        ],
                        [
                            {
                                // auto-sized columns have their widths based on their content
                                style: 'title',
                                text: 'Escolaridade',
                                border: [false, true, false, true]
                                //margin: [20, 20],
                            },
                            {
                                text: [
                                    { text: textSchoolings, style: 'description' },
                                ],
                                border: [false, false, false, true],
                                style: 'description'
                            }
                        ],
                        [
                            {
                                // auto-sized columns have their widths based on their content
                                style: 'title',
                                text: 'Cursos',
                                border: [false, true, false, true]
                                //margin: [20, 20],
                            },
                            {
                                text: [
                                    { text: textCourses, style: 'description' },
                                ],
                                border: [false, false, false, true],
                                style: 'description'
                            }
                        ],
                        [
                            {
                                // auto-sized columns have their widths based on their content
                                style: 'title',
                                text: 'Experiências',
                                border: [false, true, false, true]
                                //margin: [20, 20],
                            },
                            {
                                text: textExperiences,
                                border: [false, false, false, true],
                                style: 'description'
                            }
                        ],
                        [
                            {
                                // auto-sized columns have their widths based on their content
                                style: 'title',
                                text: 'Qualificações e informações adicionais',
                                border: [false, false, false, false]
                                //margin: [20, 20],
                            },
                            {
                                text: [
                                    { text: textExtraInfos, style: 'description' },
                                ],
                                border: [false, false, false, false],
                                style: 'description'
                            }
                        ]
                    ],
                },
                layout: {
                    defaultBorder: false,
                }
            }
        ],
        styles: {
            mainStyle: {
                fontSize: 12,
                bold: true,
                border: '1px solid black',
                defaultBorder: false
            },
            title: {
                fontSize: 12,
                bold: true,
                italics: true,
                margin: [10, 10, 10, 10]
            },
            name: {
                fontSize: 18,
                bold: true,
                color: 'black',
                alignment: 'left',
            },
            header: {
                bold: true,
                fontSize: 12
            },
            description: {
                fontSize: 10,
                bold: false,
                alignment: 'left',
                margin: [0, 10, 0, 10]
            },
            headerExperience: {
                fontSize: 12,
                bold: true
            }

        }
    };
}
module.exports = {
    getTemplate
};