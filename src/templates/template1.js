const moment = require('moment')
const getTemplate = (body) => {
    let schoolings = JSON.parse(body.schoolings)
    let courses = JSON.parse(body.courses)
    let experiences = JSON.parse(body.experiences)
    let extra_infos = JSON.parse(body.extra_infos)
    var { name, cep, street, number, district, city, state, phone1, phone2, birthday, civil_state, email, goal } = body
    if (!name) name = ''
    if (!cep) cep = ''
    if (!street) street = ''
    if (!number) number = ''
    if (!district) district = ''
    if (!city) city = ''
    if (!state) state = ''
    if (!phone1) phone1 = ''
    if (!phone2) phone2 = ''
    if (!birthday) birthday = ''
    if (!civil_state) civil_state = ''
    if (!email) email = ''
    if (!goal) goal = ''
    birthday = moment.utc(birthday).format('DD/MM/YYYY')
    const address = `${street}, ${number} - ${district} - ${city} - ${state} - ${cep}`
    let textSchoolings = []
    let textCourses = []
    let textExperiences = []
    let textExtraInfos = []

    schoolings.schoolings.forEach(s => {
        var { status, level, course, instituition } = s
        level += ' - '
        status += ' | '
        course += ' - '
        if (s.level == '' || !s.level) level = 'Nível - '
        if (s.status == '' || !s.status) status = 'Status | '
        if ((s.course == '' || !s.course) && (s.level == 'Ensino Fundamental' || s.level == 'Ensino Médio')) course = ''
        if ((s.course == '' || !s.course) && (s.level !== 'Ensino Fundamental' || s.level !== 'Ensino Médio')) course = 'Curso'
        if (s.instituition == '' || !s.instituition) instituition = 'Instituição'
        textSchoolings.push({
            text: `${level} ${status} ${course} ${instituition}`,
            style: 'listSchooling'
        })
    })
    courses.courses.forEach(c => {
        var { course, instituition, year } = c
        instituition = ' - ' + instituition
        if (c.course == '' || !c.course) course = ''
        if (c.instituition == '' || !c.instituition) instituition = ''
        textCourses.push( {
            text: `${course} ${instituition} (${year})`,
            style: 'listSchooling'
        })
    })
    experiences.experiences.forEach(e => {
        var listExperience = []
        textExperiences.push({
            text: `(${e.init}) (${e.end}) \n`,
            style: 'experienceDate'
        })
        textExperiences.push({
            text: `${e.local} \n`,
            style: 'experienceLocal'
        })
        e.items.forEach(i => {
            listExperience.push({
                text: `${i}`,
                style: 'listExperience'
            })
        })
        textExperiences.push({
            ul: listExperience
        })
        
        listExperience = []
    })
    extra_infos.extra_infos.forEach(i => {
        textExtraInfos.push({
            text: `${i}`,
            style: 'listSchooling'
        })
    })
    return docDefinition = {
        content: [
            {
                text: `${name} \n`,
                style: 'name'
            },
            {
                text: `${address} - ${phone1} - ${email} \n`,
                style: 'personalInfos'
            },
            {
                text: 'Objetivo \n',
                style: 'title'
            },
            {
                text: `${goal} \n`,
                style: 'goal'
            },
            {
                text: 'Formação \n',
                style: 'title'
            },
            {
                ul: textSchoolings
            },
            textCourses.length > 0 ? {
                text: 'Cursos \n',
                style: 'title'
            }: '',
            {
                ul: textCourses
            },
            textExperiences.length > 0 ? {
                text: 'Experiência \n',
                style: 'title'
            } : '',
            textExperiences,
            textExtraInfos.length > 0 ? {
                text: 'Informações Adicionais \n',
                style: 'title'
            }: '',
            {
                ul: textExtraInfos,
            }
        ],
        styles: {
            name: {
                fontSize: 24,
                bold: true,
                margin: [0, 0, 0, 0]
            },
            personalInfos: {
                fontSize: 10,
                margin: [0, 0, 0, 10]
            },
            title: {
                fontSize: 14,
                margin: [0, 20, 0, 10]
            },
            goal: {
                fontSize: 14,
                color: 'gray'
            },
            listSchooling: {
                color: 'gray',
                fontSize: 10,
                margin: [10, 0, 0, 0]
            },
            experienceDate: {
                fontSize: 10,
            },
            experienceLocal: {
                fontSize: 12,
                margin: [0, 0, 0, 10]
            },
            listExperience: {
                fontSize: 10,
                color: 'gray',
                margin: [10, 0, 0, 0]
            }
        }
    }
}
module.exports = {
    getTemplate
};